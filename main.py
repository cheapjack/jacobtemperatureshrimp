# Complete project details at https://RandomNerdTutorials.com

# Import the libraries we need
from machine import Pin, I2C
import ssd1306
import onewire
import ds18x20
from time import sleep

# ESP32 Pin assignment 
#i2c = I2C(-1, scl=Pin(22), sda=Pin(21))

# ESP8266 Screen Pin assignment
i2c = I2C(-1, scl=Pin(5), sda=Pin(4))

# Define screen width and a screen object
oled_width = 128
oled_height = 64
oled = ssd1306.SSD1306_I2C(oled_width, oled_height, i2c)

# Define the pins for temp sensor
ds_pin = Pin(1)
ds_sensor = ds18x20.DS18X20(onewire.OneWire(ds_pin))
# Wait for DS sensor to warm up
sleep(0.25)
# Scan them and assign to roms
roms = ds_sensor.scan()
# Conver to str with str() function
my_roms_string = str(roms)
# Send the text to the screen object 'string', x pos, y pos
oled.text('Hello, Jacob!', 0, 0)
oled.text('DS devices: ', 0, 10)
oled.text(my_roms_string, 0, 20)
oled.text('Temperature:', 0, 30)
#oled.text('is...', 15, 30)
# Show it
oled.show()
# Pause for stability
sleep(2)

ds_sensor.convert_temp()
sleep(0.25)
for rom in roms:
    ds_sensor_reading_string = str(ds_sensor.read_temp(rom))
    oled.text(ds_sensor_reading_string, 0, 40)
    oled.show()
