## DS18B20 Temperature reading & OLED display
![documentation status](https://img.shields.io/badge/documentation-complete-green.svg)

<a href="http://www.wtfpl.net/"><img
       src="http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-4.png"
              width="80" height="15" alt="WTFPL" /></a>

<img src="images/TempShrimp.jpg" width="600">
<img src="Fritzing_Schem/ESP8266_OLED_DS18B20_bb.png" width="600">

[Micropython](https://docs.micropython.org/en/latest/esp8266/quickref.html) running on a cheap $4 [ESP8266 CP2102](https://www.aliexpress.com/item/1005001636634198.html) board to read air temperature for Geography transects, using the cheap and popular one-wire [DS18B20](https://www.ebay.co.uk/itm/133395747790) waterproof digital temperature sensor and display on a cheap £4 [128x64 OLED I2C screen](https://www.ebay.co.uk/itm/128x64-white-I2C-OLED-display-for-Arduino-or-other-microcontroller-SSD1306/224367466190)

## Rationale

Cheapness and ease of documentation mean communities can monitor temperature variance with minimal costs and some volunteers.

## Thanks

Based on these excellent tutorials

 * https://randomnerdtutorials.com/micropython-ds18b20-esp32-esp8266/

 * https://randomnerdtutorials.com/micropython-oled-display-esp32-esp8266/

And with all we learnt at the [DoESLiverpool](https://doesliverpool.com/) makerspace [Wearable Electronics Workshop](github.com/DoESLiverpool/WearableTechBadgeWorkshop)

Consider ugrading to web based sensor

 * https://randomnerdtutorials.com/esp8266-nodemcu-mqtt-publish-ds18b20-arduino/

## Install

Follow these tutorials to flash the micropython `.bin` file included in the micropython directory in this repo

### ESPTOOL

[Learn how to](https://randomnerdtutorials.com/flashing-micropython-firmware-esptool-py-esp32-esp8266/) Wipe and write fresh micropython firmware 

### AMPY

[Learn how to](https://learn.adafruit.com/micropython-basics-load-files-and-run-code/install-ampy) use ampy to send files to your ESP8266

